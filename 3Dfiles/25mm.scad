// The old world square bases

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

t = 0.8 ; // tolerance - little bit to accommodate extrusion innaccuracies, and tight movement trays
bh = 3.5 ; // base height you are clipping on to
bx = 25 ; // base x dimension - base width 
wt = 1.5 ; // wall thicknesses
bd = 2 ; // base diff - how much wider the bottom of the base is from the top
md = 5 ; // magnet diameter

// gw square 20mm base - no slot, GW C 2005 stamped on underside
// 17.5mm across on the top
// 19.72mm across on the bottom
// 3.51mm high
// @ 23.9C and 54% humidity

// calculated values - do not edit by hand
nx = bx-2*wt ; // negative x dimension - base width of the cavity
btx = bx - bd ; // base top x dimension - base top width 
bxr = (bx-t)/2 ; // base radius for using squarebase to make the base
btxr = (btx-t)/2 ; // base top radius for using cones to make the base

nxr = nx/2 ; // cavity radius for using cones module
ntxr = (nx-bd)/2 ; // cavity top x radius, making same relative width difference to maintain slope

// modules

// squarebase is just cone_outer from https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/undersized_circular_objects with fn = 4
module squarebase(height,radius1,radius2){
   fudge = 1/cos(180/4);
   cylinder(h=height,r1=radius1*fudge,r2=radius2*fudge,$fn=4);}
   
// global operations
scale([sf,sf,sf])
// flip it and pull back up to origin plane
translate([0,0,bh])
rotate([180,0,45])  

//object definitions

union(){
  
// pop a magnet slot on the underside of the base 
difference(){
  cylinder(bh,r=md/2+wt+t,$fn=360);
  cylinder(bh,r=md/2+t,$fn=360);
  }


// cut the cavity out of the base
difference(){
            
            // base
            squarebase(bh,bxr,btxr);
            
            translate([0,0,-bd/2])
            // original base
            squarebase(bh,nxr,ntxr);
            }
        }