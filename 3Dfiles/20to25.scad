// The old world base clip on extender - 20 to 25mm

// 2024-01-11: v0.6 - most iterations v0.4-0.6 have been just tweaking 't', tolerance, 
// for my printer's settings and various generations of citadel 20mm 
// base minor variations

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

t = 0.8 ; // tolerance - extra little bit added from what you chop out, to make other things fit - increase if your extender is too tight, decrease if it's too loose
bh = 3.5 ; // base height you are clipping on to
bx = 19.72 ; // base x dimension - base width you are clipping on to - see below for my measurements metadata
btx = 17.5 ; // base top x dimension - base top width you are clipping on to
by = 19.72 ; // base y dimension - base depth you are clipping on to
bty = 17.5 ; // base top y dimension - base top depth you are clipping on to

nx = 25 ; // new x dimension - base width of the final base after the clip is on
ny = 25 ; // new y dimension - base depth of the final base after the clip is on

// gw square 20mm base - no slot, GW C 2005 stamped on underside
// 17.5mm across on the top
// 19.72mm across on the bottom
// 3.51mm high
// @ 23.9C and 54% humidity

// calculated values - do not edit by hand
br = (bx+t)/2 ; // base radius for using squarebase to make the base
btr = (btx+t)/2 ; // base top radius for using cones to make the base
bth = bh+t ; // base top height with tolerance
nxr = nx/2 ; // new radius for using cones module
ntxr = (nx-bx+btx)/2 ; // new top x radius, making same relative width difference to maintain slope

//modules

// squarebase is just cone_outer from https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/undersized_circular_objects with fn = 4
module squarebase(height,radius1,radius2){
   fudge = 1/cos(180/4);
   cylinder(h=height,r1=radius1*fudge,r2=radius2*fudge,$fn=4);}
   
//global operations
scale([sf,sf,sf])
translate([0,0,bh])
rotate([180,0,45])  
//object definitions

// cut the smaller base out of the final base size, then flip it for printing

difference(){
            
            // final base
            squarebase(bh+t/2,nxr,ntxr);
            
            translate([0,0,t/4])
            // original base
            squarebase(bh+t/2,br,btr);
            
            //cut the flap out
            rotate([0,0,45])
            cube([bx-t/2,by-t/2,2*t],center=true);
            
            // ... leaving just clips on one side
            rotate([0,0,45])
            translate([0,by/2-by/8+t/2,0])
            cube([bx+t,by/4,t],center=true);
            
            // ... leaving just clips on one side
            rotate([0,0,45])
            translate([0,-(by/2-by/8+t/2),0])
            cube([bx+t,by/4,t],center=true);
            
              }
            